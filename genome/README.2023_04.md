# Xenopus tropicalis genome annotation, 2023_04 release.

## Chromosome naming

Set a chromosome/scaffold name to the UCSC style. 

  - Reference: GCF_000004195.4_UCB_Xtro_10.0_assembly_report.txt (from RefSeq)
  - Chromosome name: chr1, chr2, chr3, ..., chrM
    - XenBase uses Chr1, Chr2, Chr3, ..., MT
    - EnsEMBL uses 1, 2, 3, ..., MT
    - NCBI RefSeq uses NC_006839.1, NC_030677.2, ..., NC_006839.1
  - Scaffold name: chrUn_NW_022279347v1, chrUn_NW_022279347v1, ...
    - XenBase uses Sca1, Sca2, ...
    - EnsEMBL uses AAMC04000011.1, AAMC04000012.1, ...
    - NCBI RefSeq uses NW_022279347.1, NW_022279348.1, ...
  - Scripts to convert the chromosome names (it converts the first column of the text file, so you can use it for other format, such as BED).
    - **convert-XENTR-ens2ucsc.py** : EnsEMBL name to USCS-style name.
    - **convert-XENTR-refseq2ucsc.py** : NCBI RefSeq name to UCSC-style name.


## NCBI RefSeq 104 (a base of the XenBase) vs. EnsEMBL
  - Scripts to make a concise gene_gff3 file, having gene_id, gene_name and biotype.
    - **make-concise-ens_gene_gff3.py** : for EnsEMBL GFF3. Use a UCSC-style GFF3 as an input.
    - **make-concise-refseq_gene_gff3.py** : for NCBI RefSeq GFF3. Use a UCSC-style GFF3 as an input.

  - NCBI RefSeq 104 annotation: 28,293 genes
    - XENTR_ncbi104.gene.raw.gff3 (extracted directly from the EnsEMBL 109 GFF3)
    - XENTR_ncbi104.gene.coding.gff3 : 21,832 genes
    - XENTR_ncbi104.gene.noncoding.gff3 : 6,328 genes (tRNA, rRNA, snRNA, snoRNA, miRNA, lncRNA)
    - XENTR_ncbi104.gene.immuno.gff3 : 72 genes (C_region, V_segment)
    - XENTR_ncbi104.gene.coding.gff3 : 61 genes

  - EnsEMBL v109 annotation: 22,109 genes
    - XENTR_ens109.gene.raw.gff3 (extracted directly from the EnsEMBL 109 GFF3)
    - XENTR_ens109.gene.coding.gff3 : 22,102 genes
    - XENTR_ens109.gene.immuno.gff3 : 5 genes (IGV genes)

## gene_name_string for a simple synteny analysis
  - **XENTR_ncbi104.gene_name_string.txt** contains the order of neighboring genes, based on NCBI RefSeq 104 gene_gff3.
    - Column 1: Chromosome name
    - Column 2: Neighboring genes on 5'-side of a query gene (max 5)
    - Column 3: Query gene
    - Column 4: Neighboring genes on 3'-side of a query gene (max 5)
  - **make-gene_name_string.py** : a script to make a gene_name_string.
