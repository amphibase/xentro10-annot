#!/usr/bin/env python3

filename_ens = 'XENTR.ens109.gene.gff3.converted.coding'
#chr1	ensembl	gene	26787	30692	.	+	.	ID=gene:ENSXETG00000046667;Name=NotAvailable;biotype=protein_coding
#chr1	ensembl	gene	53013	78429	.	+	.	ID=gene:ENSXETG00000021845;Name=NotAvailable;biotype=protein_coding

filename_ncbi = 'XENTR.ncbi104.gene.gff3.converted.coding'
#chr1	Gnomon	gene	43856	87766	.	+	.	ID=GeneID:101732307;Name=LOC101732307;biotype=protein_coding
#chr1	Gnomon	gene	100440	112187	.	+	.	ID=GeneID:101731805;Name=pi16;biotype=protein_coding


def read_gff(tmp_filename):
    rv = dict()
    f = open(tmp_filename, 'r')
    for line in f:
        if line.startswith('#'):
            continue
        tokens = line.strip().split("\t")
        chr_id = tokens[0]
        start_pos = int(tokens[3])
        end_pos = int(tokens[4])
        tmp_strand = tokens[6]
        tmp_tag = tokens[8]
        if chr_id not in rv:
            rv[chr_id] = dict()
        rv[chr_id][tmp_tag] = {'start': start_pos, 'end': end_pos, 'strand': tmp_strand}
    f.close()
    return rv


genes_ens = read_gff(filename_ens)
genes_ncbi = read_gff(filename_ncbi)

for tmp_chr in genes_ncbi.keys():
    for tmp_ncbi_tag, tmp_ncbi_info in genes_ncbi[tmp_chr].items():
        tmp_ncbi_pos = '%s:%d-%d' %\
                       (tmp_chr, tmp_ncbi_info['start'], tmp_ncbi_info['end'])
        tmp_ncbi_len = tmp_ncbi_info['end'] - tmp_ncbi_info['start'] + 1

        if tmp_chr not in genes_ens:
            print("NCBI_only\t%s\t%s" % (tmp_ncbi_tag, tmp_ncbi_pos))
            continue

        count_ens_hits = 0
        for tmp_ens_tag, tmp_ens_info in genes_ens[tmp_chr].items():
            if tmp_ncbi_info['start'] > tmp_ens_info['end'] or\
                tmp_ncbi_info['end'] < tmp_ens_info['start']:
                continue

            count_ens_hits += 1
            tmp_ens_pos = '%s:%d-%d' %\
                       (tmp_chr, tmp_ens_info['start'], tmp_ens_info['end'])
            tmp_ens_len = tmp_ens_info['end'] - tmp_ens_info['start'] + 1
            
            tmp_overlap_len = min([tmp_ncbi_info['end'], tmp_ens_info['end']]) - max([tmp_ncbi_info['start'], tmp_ens_info['start']]) + 1
            # ncbi_start < ens_start < ens_end < ncbi_end
            if tmp_ncbi_info['start'] <= tmp_ens_info['start'] and\
                tmp_ncbi_info['end'] >= tmp_ens_info['end']:
                print("ENS_in_NCBI\t%d\t%d\t%d\t%s\t%s\t%s\t%s" % (tmp_ncbi_len, tmp_ens_len, tmp_overlap_len, tmp_ncbi_tag, tmp_ncbi_pos, tmp_ens_tag, tmp_ens_pos))
            
            # ens_start < ncbi_start < ncbi_end < ens_end
            elif tmp_ncbi_info['start'] >= tmp_ens_info['start'] and\
                tmp_ncbi_info['end'] <= tmp_ens_info['end']:
                print("NCBI_in_ENS\t%d\t%d\t%d\t%s\t%s\t%s\t%s" % (tmp_ncbi_len, tmp_ens_len, tmp_overlap_len, tmp_ncbi_tag, tmp_ncbi_pos, tmp_ens_tag, tmp_ens_pos))
            
            # ens_start < ncbi_start < ens_end < ncbi_end 
            elif tmp_ncbi_info['start'] >= tmp_ens_info['start'] and\
                tmp_ncbi_info['end'] >= tmp_ens_info['end']:
                print("ENS_overlap_NCBI\t%d\t%d\t%d\t%s\t%s\t%s\t%s" % (tmp_ncbi_len, tmp_ens_len, tmp_overlap_len, tmp_ncbi_tag, tmp_ncbi_pos, tmp_ens_tag, tmp_ens_pos))
            
            # ncbi_start < ens_start < ncbi_end < ens_end 
            elif tmp_ncbi_info['start'] <= tmp_ens_info['start'] and\
                tmp_ncbi_info['end'] <= tmp_ens_info['end']:
                print("NCBI_over_ENS\t%d\t%d\t%d\t%s\t%s\t%s\t%s" % (tmp_ncbi_len, tmp_ens_len, tmp_overlap_len, tmp_ncbi_tag, tmp_ncbi_pos, tmp_ens_tag, tmp_ens_pos))

            else:
                print("Unknown_over\t%d\t%d\t%d\t%s\t%s\t%s\t%s" % (tmp_ncbi_len, tmp_ens_len, tmp_overlap_len, tmp_ncbi_tag, tmp_ncbi_pos, tmp_ens_tag, tmp_ens_pos))
        
        if count_ens_hits == 0:
            print("NCBI_only\t%d\t0\t0\t%s\t%s\tNA\tNA" % (tmp_ncbi_len, tmp_ncbi_tag, tmp_ncbi_pos))


for tmp_chr in genes_ens.keys():
    for tmp_ens_tag, tmp_ens_info in genes_ens[tmp_chr].items():
        tmp_ens_pos = '%s:%d-%d' %\
                       (tmp_chr, tmp_ens_info['start'], tmp_ens_info['end'])
        tmp_ens_len = tmp_ens_info['end'] - tmp_ens_info['start'] + 1

        if tmp_chr not in genes_ncbi:
            print("ENS_only\t0\t%d\t0\tNA\tNA\t%s\t%s" % (tmp_ens_len, tmp_ens_tag, tmp_ens_pos))
            continue

        count_ncbi_hits = 0
        for tmp_ncbi_tag, tmp_ncbi_info in genes_ncbi[tmp_chr].items():
            if tmp_ens_info['start'] > tmp_ncbi_info['end'] or\
                tmp_ens_info['end'] < tmp_ncbi_info['start']:
                continue
            count_ncbi_hits += 1
        
        if count_ncbi_hits == 0:
            print("ENS_only\t0\t%d\t0\tNA\tNA\t%s\t%s" % (tmp_ens_len, tmp_ens_tag, tmp_ens_pos))
