#!/usr/bin/env python3
import sys

filename_gff = sys.argv[1]

# ==> XENTR.ens109.gene.gff3.converted <==
# chr1	ensembl	gene	26787	30692	.	+	.	ID=gene:ENSXETG00000046667;biotype=protein_coding;gene_id=ENSXETG00000046667;logic_name=ensembl;version=1
# chr1	ensembl	gene	53013	78429	.	+	.	ID=gene:ENSXETG00000021845;biotype=protein_coding;gene_id=ENSXETG00000021845;logic_name=ensembl;version=5


f_coding = open('%s.coding' % filename_gff, 'w')
f_immuno = open('%s.immuno' % filename_gff, 'w')
f_other = open('%s.other' % filename_gff, 'w')

f_gff = open(filename_gff, 'r')
for line in f_gff:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
    tokens = line.strip().split("\t")

    tmp_biotype = 'NA'
    tmp_id = 'NA'
    tmp_name = 'NotAvailable'

    for tmp in tokens[8].split(';'):
        if tmp.startswith('ID='):
            tmp_id = tmp.split('=')[1]
        elif tmp.startswith('Name='):
            tmp_name = tmp.split('=')[1]
        elif tmp.startswith('biotype='):
            tmp_biotype = tmp.split('=')[1]
    
    if tmp_id == 'NA':
        sys.stderr.write('GeneID is not available. Exit.\n')
        sys.stderr.write('%s\n' % line.strip())
        sys.exit(1)
    
    if tmp_biotype == 'protein_coding':
        f_coding.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))
    elif tmp_biotype in ['IG_V_gene']:
        f_immuno.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))
    else:
        f_other.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))

f_gff.close()
f_coding.close()
f_immuno.close()
f_other.close()
