#!/usr/bin/env python3
import sys

filename_gff = sys.argv[1]

#      1 other
#      7 C_region
#     18 guide_RNA
#     42 misc_RNA
#     65 V_segment
#    187 miRNA
#    342 snoRNA
#    463 rRNA
#    614 snRNA
#   2356 lncRNA
#   2366 tRNA
#  21832 protein_coding


f_coding = open('%s.coding' % filename_gff, 'w')
f_non_coding = open('%s.noncoding' % filename_gff, 'w')
f_immuno = open('%s.immuno' % filename_gff, 'w')
f_other = open('%s.other' % filename_gff, 'w')

f_gff = open(filename_gff, 'r')
for line in f_gff:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
    tokens = line.strip().split("\t")

    tmp_biotype = 'NA'
    tmp_id = 'NA'
    tmp_name = 'NotAvailable'

    for tmp in tokens[8].split(';'):
        if tmp.startswith('Dbxref='):
            for tmp2 in tmp.split('=')[1].split(','):
                if tmp2.startswith('GeneID:'):
                    tmp_id = tmp2
        elif tmp.startswith('Name='):
            tmp_name = tmp.split('=')[1]
        elif tmp.startswith('gene_biotype='):
            tmp_biotype = tmp.split('=')[1]
    
    if tmp_id == 'NA':
        sys.stderr.write('GeneID is not available. Exit.\n')
        sys.stderr.write('%s\n' % line.strip())
        sys.exit(1)
    
    if tmp_biotype == 'protein_coding':
        f_coding.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))
    elif tmp_biotype in ['miRNA', 'tRNA', 'rRNA', 'lncRNA', 'snRNA', 'snoRNA']:
        f_non_coding.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))
    elif tmp_biotype in ['C_region', 'V_segment']:
        f_immuno.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))
    else:
        f_other.write('%s\tID=%s;Name=%s;biotype=%s\n' % 
                        ("\t".join(tokens[:8]), tmp_id, tmp_name, tmp_biotype))

f_gff.close()
f_coding.close()
f_non_coding.close()
f_immuno.close()
f_other.close()
