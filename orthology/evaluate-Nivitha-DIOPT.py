#!/usr/bin/env python3
import gzip

# A Human-XENTR DIOPT source file from Nivitha Sundararaj, 2023-MAR-23
filename_source = 'Mod_parsed_XT_HS_All_Diopt_Tool_Data.txt.gz'
filename_base = 'Nivitha_DIOPT.XT-HS.2023_03_29'

# The format of the DIOPT source file
# XT	HS	ortho	swiftortho	phylome	proteinortho	Inparanoid	Sonic	FastOrtho	XT_Gene_ID	XB_Gene_ID	HS_Gene_ID
# zfp160	"""POGK"""	1	0	0	0	0	0	0	549271	XB-GENE-5962136	57645
# zfp2	"""POGK"""	1	0	0	0	0	0	0	779838	XB-GENE-6038858	57645

xt2hs = dict()
xt2hs_phylome = dict()

f_list = gzip.open(filename_source, 'rt')
f_list.readline()
for line in f_list:
    tokens = line.strip().split("\t")
    tmp_xt = tokens[0]
    tmp_hs = tokens[1].replace('"""', '')
    tmp_diopt_count = sum([int(x) for x in tokens[2:9]])
    tmp_phylome_hit = 'PhylomeNoHit'
    if tokens[4] == '1':
        tmp_phylome_hit = 'PhylomeHit'
    if tmp_xt not in xt2hs:
        xt2hs[tmp_xt] = dict()
        xt2hs_phylome[tmp_xt] = dict()
    xt2hs[tmp_xt][tmp_hs] = tmp_diopt_count
    xt2hs_phylome[tmp_xt][tmp_hs] = tmp_phylome_hit
f_list.close()

xt2diff_list = dict()
out_list = dict()

diff_count_list = {'all': dict(), 'Matched': dict(),
                   'Different': dict(), 'Partial': dict()}
for tmp_k, tmp_v in diff_count_list.items():
    for i in range(0, 8):
        tmp_v[i] = 0

f_all = open('%s.all.txt' % filename_base, 'w')
h_line = '# XT_name\tNameMatching\tPhylomeSupport\tBest-Second\t'
h_line += 'BestHsSupport\tBestHsName\tSecondHsSupport\tSecondHsName'
f_all.write('%s\n' % h_line)
for tmp_xt in xt2hs.keys():
    tmp_hs_list = sorted(xt2hs[tmp_xt].keys(), key=xt2hs[tmp_xt].get)
    tmp_hs_first = tmp_hs_list[-1]
    tmp_hs_first_count = xt2hs[tmp_xt][tmp_hs_first]
    tmp_hs_second = 'NA'
    tmp_hs_second_count = 0
    if len(tmp_hs_list) > 1:
        tmp_hs_second = tmp_hs_list[-2]
        tmp_hs_second_count = xt2hs[tmp_xt][tmp_hs_second]

    # Filter out the case if less than 3 databases were support the orthology
    if tmp_hs_first_count < 3:
        continue

    is_matched = 'Different'
    xt_name = tmp_xt.upper()
    hs_name = tmp_hs_first.upper()

    if xt_name == hs_name:
        is_matched = 'Matched'
    elif xt_name.startswith(hs_name) or hs_name.startswith(xt_name):
        is_matched = 'Partial'

    tmp_diff_count = tmp_hs_first_count - tmp_hs_second_count
    diff_count_list['all'][tmp_diff_count] += 1
    diff_count_list[is_matched][tmp_diff_count] += 1
    xt2diff_list[tmp_xt] = tmp_diff_count

    tmp_phylome = xt2hs_phylome[tmp_xt][tmp_hs_first]
    out_list[tmp_xt] = "%s\t%s\t%s\t%d\t%d\t%s\t%d\t%s" %\
                      (tmp_xt, is_matched, tmp_phylome, tmp_diff_count,
                       tmp_hs_first_count, tmp_hs_first,
                       tmp_hs_second_count, tmp_hs_second)
    f_all.write('%s\n' % out_list[tmp_xt])
f_all.close()

## Evaluate the count difference between the best and the second best.
for tmp_matched in ['all', 'Matched', 'Partial', 'Different']:
    tmp_sum = 0
    tmp_total = sum(diff_count_list[tmp_matched].values())
    for i in range(7,-1,-1):
        tmp_count = diff_count_list[tmp_matched][i]
        tmp_sum += tmp_count
        tmp_pct = tmp_sum / tmp_total * 100.0
        print("%s: diff_count>=%d --> total=%d, cum_sum=%d (%.3f pct), missed=%d" %
              (tmp_matched, i, tmp_total, tmp_sum, tmp_pct, tmp_total-tmp_sum))

# Choose diff_count >= 2 based on the following numbers.
# 
# all: diff_count>=2 --> total=14991, cum_sum=14307 (95.437 pct), missed=684
# Matched: diff_count>=2 --> total=13175, cum_sum=12976 (98.490 pct), missed=199
# Partial: diff_count>=2 --> total=376, cum_sum=336 (89.362 pct), missed=40
# Different: diff_count>=2 --> total=1440, cum_sum=995 (69.097 pct), missed=445

f_good = open('%s.good.txt' % filename_base, 'w')
f_good.write('%s\n' % h_line)
f_review = open('%s.for_review.txt' % filename_base, 'w')
f_review.write('%s\n' % h_line)
for tmp_xt in sorted(xt2diff_list.keys(), key=xt2diff_list.get, reverse=True):
    tmp_line = out_list[tmp_xt]
    tmp_matched = tmp_line.split("\t")[1]
    if tmp_matched == 'Different':
        f_review.write('%s\n' % tmp_line)
    elif xt2diff_list[tmp_xt] < 2:
        f_review.write('%s\n' % tmp_line)
    else:
        f_good.write('%s\n' % tmp_line)
f_good.close()
f_review.close()


