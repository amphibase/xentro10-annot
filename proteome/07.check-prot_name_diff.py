#!/usr/bin/env python3
import sys

new_names = dict()
f_names = open('GeneNameChanges.DIOPT.2023_04.txt', 'r')
# OldName	NewName	Remark	Reviewed
# ATP6	mt-atp6	NewHGNC	TK
# c10orf53	c7h10orf53	ORF_naming	TK
f_names.readline()
for line in f_names:
    tokens = line.strip().split("\t")
    new_names[tokens[0]] = tokens[1]
f_names.close()

# filename_fa = 'XENTR_xenTro10.XB2023_04.raw.prot_diff_name.fa'
# >znrd1|XP_004913117.2|XM_004913060.4|GeneID:100379834 xb_gene_id=XB-GENE-5797380 uniprot=polr1h|A0A6I8RIK6
# filename_top2 = 'XENTR_xenTro10.XB2023_04.raw.prot_diff_name.GENCODE_HUMANv43_prot_all.pblat_top2'

filename_top2 = 'XENTR_xenTro10.XB2023_04.raw.prot_combined.GENCODE_HUMANv43_prot_all.id50.pblat_top2'
filename_fa = 'XENTR_xenTro10.XB2023_04.raw.prot_combined.fa'

ncbi2up = dict()
f_fa = open(filename_fa, 'r')
for line in f_fa:
    if line.startswith('>'):
        tokens = line.strip().split()
        ncbi_id = tokens[0].lstrip('>')
        xb_id = tokens[1]
        uniprot_id = tokens[2]
        ncbi2up[ncbi_id] = uniprot_id
f_fa.close()


mapped = dict()
f_top2 = open(filename_top2, 'r')
f_top2.readline()
for line in f_top2:
    tokens = line.strip().split("\t")
    q_id = tokens[0]
    t1_id = tokens[2]
    t1_ratio = float(tokens[5])

    ncbi_name = q_id.split('|')[0]
    up_name = ncbi2up[q_id].split('|')[0].replace('uniprot=', '')
    hs_name = t1_id.split('|')[0].lower()
    
    mapped[q_id] = 1

    tmp_tag = 'NotClear'
    if ncbi_name == up_name:
        tmp_tag = 'Match2'
        if ncbi_name == hs_name:
            tmp_tag = 'Match3'
    elif ncbi_name == hs_name:
        tmp_tag = 'NCBI_win'
    elif up_name == hs_name:
        tmp_tag = 'UniProt_win'
    
    new_name = ncbi_name
    if ncbi_name in new_names:
        new_name = new_names[ncbi_name]
        tmp_tag = "%s.changes" % tmp_tag
        del new_names[ncbi_name]
    print("%s\t%s\t%s\t%s\t%s\t%.3f\t%s\t%s\t%s" % (tmp_tag, new_name, ncbi_name, up_name, hs_name, t1_ratio, q_id, ncbi2up[q_id], t1_id))
f_top2.close()

for tmp_ncbi_id, tmp_up_id in ncbi2up.items():
    if tmp_ncbi_id not in mapped:
        ncbi_name = tmp_ncbi_id.split('|')[0]
        up_name = tmp_up_id.split('|')[0].replace('uniprot=', '')
        
        tmp_tag = 'NotClear.NoHs'
        if ncbi_name == up_name:
            tmp_tag = 'Match2.NoHs'
        new_name = ncbi_name
        if ncbi_name in new_names:
            new_name = new_names[ncbi_name]
            tmp_tag = "%s.changes" % tmp_tag
            del new_names[ncbi_name]
        print("%s\t%s\t%s\t%s\tNA\t0.000\t%s\t%s\tNA" % (tmp_tag, new_name, ncbi_name, up_name, q_id, ncbi2up[q_id]))


for tmp_old, tmp_new in new_names.items():
    sys.stderr.write('NotAppliedName\t%s\t%s\n' % (tmp_old, tmp_new))
