#!/usr/bin/env python
import os
import sys
import gzip

filename_out = 'XENTR_xenTro10.XB2023_03'
filename_out_fa = '%s.pep_raw.fa' % filename_out
filename_out_log = '%s.pep_raw.log' % filename_out

filename_gff = 'XENTR_10.0_Xenbase.gff3.gz'
# Chr1	Xenbase	gene	43856	87766	.	+	.	ID=XBXT10g003319;Name=LOC101732307;Dbxref=GeneID:101732307;gbkey=Gene;gene=LOC101732307;gene_biotype=protein_coding
# Chr1	Xenbase	gene	100439	112192	.	+	.	ID=XBXT10g015076;Name=pi16;Dbxref=Xenbase:XB-GENE-22068880,GeneID:101731805;gbkey=Gene;gene=pi16;gene_biotype=protein_coding
# Chr1	Xenbase	gene	130008	136889	.	-	.	ID=XBXT10g010053;Name=dok1;Dbxref=GeneID:100170612,Xenbase:XB-GENE-5968250;gbkey=Gene;gene=dok1;gene_biotype=protein_coding

filename_fa = 'XENTR_10.0_Xenbase.pep.fa'
# >XBXT10g017325|mRNA085669| sell
# >XBXT10g017578|mRNA086760| LOC101730422
# >XBXT10g029061|mRNA101839| hspa1b


def is_file_accessible(tmp_filename):
    if not os.access(tmp_filename, os.R_OK):
        sys.stderr.write('%s does not exist. Exit.\n\n' % tmp_filename)
        sys.exit(1)


is_file_accessible(filename_gff)
is_file_accessible(filename_fa)

locus_info = dict()

f_gff = gzip.open(filename_gff, 'rt')
for line in f_gff:
    tokens = line.strip().split("\t")
    tmp_type = tokens[2]
    if tmp_type != 'gene':
        continue
    locus_id = 'NA'
    gene_name = 'NA'
    xb_gene_id = 'NA'
    entrez_gene_id = 'NA'
    for tmp_tokens in tokens[8].split(';'):
        if tmp_tokens.startswith('ID='):
            locus_id = tmp_tokens.split('=')[1]
        if tmp_tokens.startswith('Name='):
            gene_name = tmp_tokens.split('=')[1]
        if tmp_tokens.startswith('Dbxref='):
            for tmp_tokens2 in tmp_tokens.split('=')[1].split(','):
                if tmp_tokens2.startswith('Xenbase:'):
                    xb_gene_id = tmp_tokens2.split(':')[1]
                if tmp_tokens2.startswith('GeneID:'):
                    entrez_gene_id = tmp_tokens2.split(':')[1]

    if locus_id not in locus_info:
        locus_info[locus_id] = {'gene_name': gene_name,
                                'xb_gene_id': xb_gene_id,
                                'entrez_gene_id': entrez_gene_id}
    else:
        sys.stderr.write("Duplicate record: %s\n" % locus_id)
f_gff.close()

sys.stderr.write("Number of loci(genes) in GFF3: %d\n" % len(locus_info))

AA_set = set('ARNDCEQGHILKMFPSTWYV')
AA_set = set('UO').union(AA_set)

seq_list = dict()

f_out = open(filename_out_fa, 'w')
f_log = open(filename_out_log, 'w')

f_fa = open(filename_fa, 'r')
for line in f_fa:
    if line.startswith('>'):
        tmp_h_tokens = line.strip().lstrip('>').split()
        tmp_name = 'NA'
        if len(tmp_h_tokens) > 1:
            tmp_name = tmp_h_tokens[1]
        tmp_h_tokens2 = tmp_h_tokens[0].split('|')
        tmp_locus_id = tmp_h_tokens2[0]
        if tmp_name != locus_info[tmp_locus_id]['gene_name']:
            f_log.write("ERROR-GeneName: %s (fasta=%s, gff3=%s)\n" %
                        (tmp_locus_id, tmp_name,
                         locus_info[tmp_locus_id]['gene_name']))

        tmp_l = locus_info[tmp_locus_id]
        f_out.write(">%s|XB:%s|GeneID:%s|%s\n" %
                    (tmp_name, tmp_l['xb_gene_id'],
                     tmp_l['entrez_gene_id'], tmp_locus_id))
    else:
        tmp_seq = line.strip().rstrip('*')
        if not set(tmp_seq).issubset(AA_set):
            f_log.write("ERROR-UnusualAA: %s (%s)\n" % (tmp_name, tmp_seq))
        if tmp_seq != '':
            f_out.write("%s\n" % tmp_seq)
f_fa.close()

f_log.close()
f_out.close()
