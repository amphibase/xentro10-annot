#!/usr/bin/env python3

filename_list = []
filename_list.append('XENTR_xenTro10.XB2023_04.raw.prot_NCBI_only.included')
filename_list.append('XENTR_xenTro10.XB2023_04.raw.prot_UniProt_only.included')

for tmp_filename in filename_list:
    tmp_list = []
    f = open(tmp_filename, 'r')
    for tmp_line in f:
        tmp_list.append(tmp_line.strip())
    f.close()

    f_out = open(tmp_filename.replace('.included', '.unique.fa'), 'w')
    f_fa = open(tmp_filename.replace('.included', '.fa'), 'r')
    is_print = -1
    for line in f_fa:
        if line.startswith('>'):
            tmp_h = line.strip().lstrip('>').split()[0]
            if tmp_h in tmp_list:
                is_print = -1
            else:
                is_print = 1
                f_out.write("%s\n" % line.strip())
        elif is_print > 0:
            f_out.write("%s\n" % line.strip())
    f_fa.close()
    f_out.close()
